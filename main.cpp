#include <IRLibDecodeBase.h>
#include <IRLibAll.h>
#include <IRLibGlobals.h>
#include <IRLibRecvLoop.h>
#include <IRLib2.h>
#include <IRLibRecvBase.h>
#include <IRLib_P01_NEC.h>      
#include <IRLibSendBase.h> 


#define NEC 1
#define POWER_TOGGLE    0X5EA1F807
#define MUTE        0X5EA138C7
#define VOLUME_UP   0X5EA158A7
#define VOLUME_DOWN 0X5EA1D827
#define VCR1        0X5EA1F00F 
#define AUX         0X5EA1C837
#define PHONO       0X5EA128D7 
#define CD          0X5EA1A857
#define TUNER       0X5EA16897 
#define TAPE_2      0X5EA118E7  
#define TAPE_1      0X5EA19867


IRsend mySender;

//Test Buttons

const int buttonPin = 8;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int channel_state = 0;         // variable for reading the pushbutton status

//Send power toggle IR
void sendPower(){
  mySender.send(NEC,POWER_TOGGLE, 20);
}

//Mute IR
void sendMute(){
  mySender.send(NEC,MUTE, 20);
}

//Turn up the volume with IR
void sendVolUp(){
    mySender.send(NEC,VOLUME_UP, 20);
}

//TurnDown the volume IR
void sendVolDown(){
      mySender.send(NEC,VOLUME_DOWN, 20);
}

//Change the MODE. IE, change from VC1 to CD... but like a channel. 
// This code is an effort to cut down on needed buttons... 
void channelUp(){

//default to AUX.. IE on a yamaha RV1050 the far left mode. 

  switch(channel_state ) {
//   case -1 :
      //mySender.send(NEC,AUX, 20);
//      channel_state = 6;
   case 0  :
      mySender.send(NEC,AUX, 20);
      channel_state++;
      break; /* optional */
   case 1  :
      mySender.send(NEC,VCR1, 20);
      channel_state++;
      break; /* optional */
   case 2  :
      mySender.send(NEC,TAPE_2, 20);
      channel_state++;
      break; /* optional */
   case 3  :
      mySender.send(NEC,TAPE_1, 20);
      channel_state++;
      break; /* optional */
   case 4  :
      mySender.send(NEC,TUNER, 20);
      channel_state++;
      break; /* optional */
   case 5  :
      mySender.send(NEC,CD, 20);
      channel_state++;
      break; /* optional */
   case 6  :
      mySender.send(NEC,PHONO, 20);
      channel_state++;
      break; /* optional */
 //  case 7  :
      //mySender.send(NEC,PHONO, 20);
//      channel_state = 1;
 //     break; /* optional */
  
   /* you can have any number of case statements */
  // default : /* Optional */

  }
  
}

//Ditto for above but channel change down. 
void channelDown(){

  switch(channel_state ) {
//   case -1 :
//      mySender.send(NEC,AUX, 20);
///      channel_state = 0;
   case 0  :
      mySender.send(NEC,AUX, 20);
      channel_state--;
      break; /* optional */
   case 1  :
      mySender.send(NEC,VCR1, 20);
      channel_state--;
      break; /* optional */
   case 2  :
      mySender.send(NEC,TAPE_2, 20);
      channel_state--;
      break; /* optional */
   case 3  :
      mySender.send(NEC,TAPE_1, 20);
      channel_state--;
      break; /* optional */
   case 4  :
      mySender.send(NEC,TUNER, 20);
      channel_state--;
      break; /* optional */
   case 5  :
      mySender.send(NEC,CD, 20);
      channel_state--;
      break; /* optional */
   case 6  :
      mySender.send(NEC,PHONO, 20);
      channel_state--;
      break; /* optional */
//   case 7  :
 //     channel_state = 1; 
//      mySender.send(NEC,AUX, 20);
//      channel_state = 6;
//      break; /* optional */
  
   /* you can have any number of case statements */
  // default : /* Optional */

  }
}

// Borrowed code from: https://electronics.stackexchange.com/questions/101409/how-to-debouce-six-buttons-on-one-analog-pin-with-arduino
// I know nothing about debounce or whatever... 
int old_button = 0;

int getButton()
{
  int i, z, sum;
  int button;

  sum = 0;
  for (i=0; i < 4; i++)
  {
     sum += analogRead(5);
     
  }
  //Serial.println(sum);
  z = sum / 4;
  //Serial.println(z);
  if (z > 1021) button = 0;                                           
  else if (z >= 510 && z <= 514) button = 1;                     
  else if (z >= 680 && z <= 684) button = 2;                
  else if (z >= 766 && z <= 770) button = 3;                
  else if (z >= 817 && z <= 822) button = 4;             
  else if (z >= 851 && z <= 856) button = 5; 
  else if (z >= 875 && z <= 880) button = 6;
  else button = 0;

  return button;
}

void setup() {
  Serial.begin(9600);
  Serial.println("TEST!");
    // initialize the LED pin as an output:
//  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input//:
//  pinMode(buttonPin, INPUT);
}
void loop ()
{
  int button, button2, pressed_button;  
  button = getButton();
  Serial.println(channel_state);

  //Handle out of bounnds channel. 

  if (channel_state >= 7) {
    channel_state = 0; 
    mySender.send(NEC,AUX, 20);
  }
  if (channel_state <= -1 ) {
    channel_state = 6; 
    mySender.send(NEC,PHONO, 20);
  }
  
  if (button != old_button)
  {
     //Serial.println(analogRead(A0));
      delay(50);        // debounce
      button2 = getButton();

      if (button ==  button2)
      {
       //  sendPower();
         old_button = button;
         pressed_button = button;
      //   Serial.println(pressed_button);
      }

      if (pressed_button == 1)
      {
         sendPower();
       //  old_button = button;
       //  pressed_button = button;
         Serial.println(pressed_button);
      }
      if (pressed_button == 2)
      {
         sendMute();
       //  old_button = button;
      ///   pressed_button = button;
         Serial.println(pressed_button);
      }
      if (pressed_button == 3)
      {
         sendVolUp();
       //  old_button = button;
      //   pressed_button = button;
         Serial.println(pressed_button);
      }
      if (pressed_button == 4)
      {
         sendVolDown();
      //   old_button = button;
      //   pressed_button = button;
         Serial.println(pressed_button);
      }
      if (pressed_button == 5)
      {
         channelUp();
       //  old_button = button;
       //  pressed_button = button;
         Serial.println(pressed_button);
      }
      if (pressed_button == 6)
      {
         channelDown();
       //  old_button = button;
     //    pressed_button = button;
         Serial.println(pressed_button);
      }
      
   }
}
