# What
This is an IR remote control that has limited capability to control a Yamaha RX-V1050 Stereo Reciever. 

## Dependencies: 
* Adafruit Metro - Allows for powering the board off. This can save power for a 9volt batter: https://www.adafruit.com/product/2488
* 9 Volt battery and adapter cable for a DC input: https://www.adafruit.com/product/80
* Arduino protoboard with headers. Note: The board that I used are sold on amazon and are called: Electronics-Salon 10x Prototype PCB for Arduino UNO R3 Shield Board DIY. by CZH-LABS. But other boards should work. 
* Through hole buttons x 6. 
* Assorted through hole jumpers. 
* An IR transmitter.
* Transistor: https://www.onsemi.com/pub/Collateral/PN2222-D.PDF
* Resistors: 
 - 2.2KΩ ±5% [E24] x 7
 - 470Ω ±5% [E24] x 1

## How To
There is a lot of soldering in this project. 

Here is a schematic: ![Schematic.jpg](/schematic.jpg)

Here is an image: ![image.jpg](/image.jpg)

Resistors R2 through R8 are 2.2KOhms, these resistors are for button debounce. R1 is 470Ohms and is used in conjunction with the IR and transistor. 

~~The voltage coming from the PWM pin number 3 is 1200 mV DC before the 470ohm resistor, and 16mV to 25mV after. Please note: the IR LED transistor should be wired up with the PWM pin connected to `BASE` of the transistor, ground connected to the `COLLECTOR`, and the IR `-` side connected to the `EMITTER` side of the transistor, however please double check this.~~

~~The IR may be this mode: http://www.nteinc.com/specs/3000to3099/pdf/nte3017.pdf. The current IR circuit is incorrect. We need something like this: https://arduino.stackexchange.com/questions/36758/using-a-transistor-to-get-100ma-on-a-ir-led. Note the current limiting resistor on the VCC (5volt/3volt), and the current limiting resistor on the base leg of the transistor which limits the load from the PWM. The next iteration of this build will have numbers, and test results. I doubt anyone has found this repo, but if you are reading this, I recommend creating this circuit in breadboard. I burnt out the orignal IR LED, and the solder rework is not worth the pain.~~

The new circuit shown in the schematic has a 1kΩ load resistor on the emitter side of the transitor which should give us 80 to 100mA of current on the emitter side, multimeter tests to follow. 


## How does the remote work?
The first button is for power. The second for mute, third for volume up, fourth volume down, fifth channel up, sixth, channel down. The channel up and down buttons handle the modes that an RX-V1050 has like tuner, AUX, and Video. 

# Credit: 
Credit for the debounce code goes here: https://electronics.stackexchange.com/questions/101409/how-to-debouce-six-buttons-on-one-analog-pin-with-arduino

# Todo: 
1. Add a demo. 
2. Detail the soldering steps. 
3. Rework the IR LED circuit. We need more LED POWAH... the distance is kinda low. 

## Helpful Resources: 
http://www.resistorguide.com/resistor-color-code-calculator	
